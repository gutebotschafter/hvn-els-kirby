#!/usr/bin/env bash

if [[ $1 == "" ]]; then
    echo "Missing Container Parameter like php or web..."
    echo "Available Container:"
    docker ps --format "{{.Names}}"
    exit
fi

DOCKERID=$(docker ps | grep _$1_ | cut -d' ' -f1)

docker exec -it "$DOCKERID" /bin/bash
