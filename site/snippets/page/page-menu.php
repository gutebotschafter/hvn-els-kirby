<nav class="page-menu">
  <a class="page-menu__logo" href="https://www.gute-botschafter.de" title="Die Gute Botschafter Webseite besuchen" target="_blank" rel="noreferrer">
    <img src="<?= url('assets/images/logo-gb.svg') ?>" alt="Gute Botschafter">
  </a>
  <a class="page-menu__logo" href="https://www.handelsverband-nrw.de/" title="Die Handelsverband Nordrhein-Westfalen Webseite besuchen" target="_blank" rel="noreferrer">
    <img src="<?= url('assets/images/logo-hvn.svg') ?>" alt="Handelsverband Nordrhein-Westfalen">
  </a>
  <?php if($page->isHomePage()): ?>
    <a class="page-menu__icon" href="#page-contact" title="Kontakt aufnehmen">
      <?php snippet('svg/icons/mail') ?>
    </a>
  <?php else: ?>
    <a class="page-menu__icon" href="<?= $site->url() ?>" title="Die <?= $site->title()->html() ?> Hauptseite anzeigen">
      <?php snippet('svg/icons/home') ?>
    </a>
  <?php endif ?>
</nav>
