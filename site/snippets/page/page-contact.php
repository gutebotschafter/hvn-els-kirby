<aside class="page-contact" id="page-contact">
  <div class="page-contact__section">
    <?= $site->description()->kirbytext() ?>
  </div>
  <div class="page-contact__section" id="page-contact-form">

    <?php if($success): ?>
      <h3>Anfrage gesendet</h3>
      <p><strong>Eine E-Mail mit Ihrer Anfrage wurde erfolgreich versendet.</strong></p>
      <p>Vielen Dank für Ihr Interesse an der <strong>Expediton Lebendige Stadt.</strong> Wir melden uns umgehend bei Ihnen.</p>
    <?php else: ?>

      <h3>Kontaktformular</h3>

      <?php if (isset($alert['error'])): ?>
        <p><?= $alert['error'] ?></p>
      <?php endif ?>

      <form class="form" method="post" action="<?= $page->url() ?>#page-contact-form">
        <div class="form__field">
          <label class="form__field__label" for="name">Name</label>
          <input class="form__field__text" type="text" id="name" name="name" value="<?= $data['name'] ?? '' ?>" required>
          <?= isset($alert['name']) ? '<span class="form__field__error">' . html($alert['name']) . '</span>' : '' ?>
        </div>
        <div class="form__field">
          <label class="form__field__label" for="email">E-Mail</label>
          <input class="form__field__text" type="email" id="email" name="email" required>
          <?= isset($alert['email']) ? '<span class="form__field__error">' . html($alert['email']) . '</span>' : '' ?>
        </div>
        <div class="form__field">
          <label class="form__field__label" for="phone">Telefon</label>
          <input class="form__field__text" type="text" id="phone" name="phone" required>
          <?= isset($alert['phone']) ? '<span class="form__field__error">' . html($alert['phone']) . '</span>' : '' ?>
        </div>
        <div class="form__field">
          <label class="form__field__label" for="company">Unternehmen</label>
          <input class="form__field__text" type="text" id="company" name="company" required>
          <?= isset($alert['company']) ? '<span class="form__field__error">' . html($alert['company']) . '</span>' : '' ?>
        </div>
        <div class="form__field form__field--checkbox">
          <label class="form__field__label" for="member">Ich bin Mitglied im Handelsverband NRW</label>
          <input class="form__field__checkbox" type="checkbox" id="member" name="member">
        </div>
        <div class="form__field form__field--honeypot">
          <label class="form__field__label" for="website">Webseite</label>
          <input class="form__field__text" type="website" id="website" name="website">
        </div>
        <div class="form__field">
          <input class="form__field__btn" type="submit" name="submit" value="Absenden">
        </div>
      </form>

    <?php endif ?>
  </div>
</aside>
