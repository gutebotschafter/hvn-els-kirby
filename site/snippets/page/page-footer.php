<footer class="page-footer">
  <div class="page-footer__logo">
    <img src="<?= url('assets/images/logo-els.svg') ?>" alt="<?= $site->title()->html() ?>">
  </div>
  <?php if($pages->filterBy('template', 'default')->listed()->count() > 0): ?>
    <nav class="page-footer__menu">
      <?php foreach($pages->filterBy('template', 'default')->listed() as $item): ?>
        <a class="page-footer__menu__item" href="<?= $item->url() ?>" title="Die Seite <?= $item->title()->html() ?> anzeigen"><?= $item->title()->html() ?></a>
      <?php endforeach ?>
    </nav>
  <?php endif ?>
  <div class="page-footer__background"></div>
</footer>
