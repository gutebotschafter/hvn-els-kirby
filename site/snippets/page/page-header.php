<header class="page-header">
  <?php if($page->isHomePage()): ?>
    <h1 class="page-header__logo">
      <img src="<?= url('assets/images/logo-els.svg') ?>" alt="<?= $site->title()->html() ?>">
      <sub><?= $site->claim()->html() ?></sub>
    </h1>
  <?php else: ?>
    <h1 class="page-header__title">
      <img src="<?= url('assets/images/logo-els.svg') ?>" alt="<?= $site->title()->html() ?>">
      <span><?= $page->title()->html() ?></span>
    </h1>
  <?php endif ?>
  <div class="page-header__image"></div>
</header>
