<blockquote class="ce-testimonial" id="<?= $data->slug() ?>">
  <div class="ce-testimonial__author">
    <img class="ce-image__image" src="<?= $data->picture()->toFile()->thumb(['width' => 640, 'height' => 640, 'crop' => true, 'quality' => 90])->url() ?>" alt>
    <cite><?= $data->author()->kti() ?></cite>
  </div>
  <div class="ce-testimonial__text">
    <?= $data->quote()->kt() ?>
  </div>
  <div class="ce-testimonial__background"></div>
</blockquote>
