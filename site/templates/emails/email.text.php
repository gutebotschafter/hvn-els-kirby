Auf der Webseite der Expedition Lebendige Stadt ist eine neue Kontaktanfrage hinterlassen worden. Folgende Kontaktdaten wurden hinterlassen:

Name: <?= $name ?>

E-Mail: <?= $email ?>

Telefon: <?= $phone ?>

Unternehmen: <?= $company ?>

<?php if($member): ?><?= $name ?> ist Mitglied im Handelsverband NRW.<?php endif ?>
