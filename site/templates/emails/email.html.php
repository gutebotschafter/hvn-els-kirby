<p>Auf der Webseite der Expedition Lebendige Stadt ist eine neue Kontaktanfrage hinterlassen worden.</p>
<p>Folgende Kontaktdaten wurden hinterlassen:</p>

<ul>
  <li>Name: <?= $name ?></li>
  <li>E-Mail: <?= $email ?></li>
  <li>Telefon: <?= $phone ?></li>
  <li>Unternehmen: <?= $company ?></li>
</ul>

<?php if($member): ?>
  <p><?= $name ?> ist Mitglied im Handelsverband NRW.</p>
<?php endif ?>
