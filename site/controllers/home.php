<?php

return function ($kirby, $pages, $page, $site) {

  $alert = null;

    if($kirby->request()->is('POST') && get('submit')) {

    // check the honeypot
    if(empty(get('website')) === false) {
      go($page->url());
      exit;
    }

    $data = [
      'name'  => get('name'),
      'email' => get('email'),
      'phone'  => get('phone'),
      'company' => get('company'),
      'member' => get('member')
    ];

    $rules = [
      'name'  => ['required', 'min' => 3],
      'email' => ['required', 'email'],
      'phone' =>  ['required', 'min' => 3],
      'company'  => ['required', 'min' => 3]
    ];

    $messages = [
      'name'  => 'Bitte geben Sie einen Namen ein',
      'email' => 'Bitte geben Sie eine korrekte E-Mail-Adresse ein',
      'phone'  => 'Bitte hinterlassen Sie uns Ihre Telefonnummer',
      'company'  => 'Bitte tragen Sie Ihr Unternehmen ein'
    ];

    // some of the data is invalid
    if($invalid = invalid($data, $rules, $messages)) {
      $alert = $invalid;

      // the data is fine, let's send the email
      } else {
        try {
          $kirby->email([
            'template' => 'email',
            'from'     => 'noreply@gute-botschafter.de',
            'replyTo'  => $data['email'],
            'to'       => $site->email()->value,
            'subject'  => 'Neue Kontaktanfrage von ' . esc($data['name']) . ' auf der Expedition Lebendige Stadt Webseite',
            'data'     => [
              'name'      => esc($data['name']),
              'email'     => esc($data['email']),
              'phone'     => esc($data['phone']),
              'company'   => esc($data['company']),
              'member' => esc($data['member'])
            ]
          ]);

        } catch (Exception $error) {
            $alert['error'] = "Das Versenden einer E-Mail über das Formular ist leider fehlgeschlagen. Bitte versuchen Sie es zu einem späteren Zeitpunkt noch einmal.";
            var_dump ($error->getMessage());
        }

      // no exception occured, let's send a success message
      if (empty($alert) === true) {
          $success = 'Ihre Anfrage wurde erfolgreich versendet. Wir melden uns umgehend bei Ihnen.';
          $data = [];
      }
    }
  }

  return [
    'alert'   => $alert,
    'data'    => $data ?? false,
    'success' => $success ?? false
  ];

};
